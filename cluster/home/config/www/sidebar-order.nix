{ ... }: {
  order = [
    { item = "overview"; }
    { item = "media browser"; hide = true; }
    { item = "admin"; bottom = true; }
    { item = "config"; bottom = true; }
    { item = "hacs"; bottom = true; }
    { item = "map"; hide = true; }
    { item = "logbook"; hide = true; }
    { item = "history"; hide = true; }
  ];
}

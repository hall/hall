# secrets

Secrets are managed with [agenix](https://github.com/ryantm/agenix):

    nix run github:ryantm/agenix -- -e ${secret}.age
